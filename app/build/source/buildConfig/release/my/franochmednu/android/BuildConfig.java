/**
 * Automatically generated file. DO NOT MODIFY
 */
package my.franochmednu.android;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String PACKAGE_NAME = "my.franochmednu.android";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
}

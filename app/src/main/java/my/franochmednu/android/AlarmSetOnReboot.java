package my.franochmednu.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * A class that resets the alarm if there was an active one during a reboot.
 */
public class AlarmSetOnReboot extends BroadcastReceiver {

	// Helper class for non-activity tasks
	Utilities utilities = new Utilities();
	
	// Alarm variables
	private AlarmManager alarmManager;
	
	public void onReceive(Context context, Intent intent) {
		if (utilities.existValuesInSharedPreferences(context)) {
			// Get values from shared preferences
			String eventName = utilities.readStringFromSharedPreferences(context, "eventName");
			long targetTimeInMillis = utilities.readLongFromSharedPreferences(context, "targetTime");
			
			// Create an intent for the alarm and send the event name as an extra
			Intent alarmIntent = new Intent(context, AlarmReceiver.class);
			alarmIntent.putExtra("eventName", eventName);
			
			// Initialize the alarm manager with a pending intent and set the alarm
			alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
			alarmManager.set(AlarmManager.RTC_WAKEUP, targetTimeInMillis, pendingIntent);
		}
	}

}
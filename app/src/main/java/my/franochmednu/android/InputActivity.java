package my.franochmednu.android;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TimePicker;

/**
 * Activity for taking target time and event name input from user 
 * and saving it to shared preferences. A successful input will 
 * move the user to TimerActivity.
 */
public class InputActivity extends Activity {
	
	// Helper class for non-activity tasks
	Utilities utilities = new Utilities();
	
	// Splash screen variables
	private LinearLayout splashLayout;	
	private Handler mHandler = new Handler();
	private static Boolean mHasNotShownSplash = true;
	
	// Time variables
	private Calendar mTargetTime;
	private long mTargetTimeInMillis;

	// Input text field variables
	private EditText mEventNameInput;
	private String mEventName;
	private Button mEventNameOKButton;
	private Dialog mEventNameDialog;
	
	// Date picker variables
	private DatePicker mDatePicker;
	private Button mDatePickerOKButton;
	private Dialog mDatePickerDialog;
	public int mTargetYear;
	private int mTargetMonth;
	public int mTargetDay;

	// Time picker variables
	private TimePicker mTimePicker;
	private Button mTimePickerOKButton;
	private Dialog mTimePickerDialog;
	private int mTargetHour;
	private int mTargetMinute;
	
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.input);

    	// Read previous state (if any) from shared preferences
    	mEventName = utilities.readStringFromSharedPreferences(this, "eventName");
    	mTargetTimeInMillis = utilities.readLongFromSharedPreferences(this, "targetTime");
    	mHasNotShownSplash = utilities.readBooleanFromSharedPreferences(this, "hasNotShownSplash");

    	// Initialize layouts and set visibility
        splashLayout = (LinearLayout) findViewById(R.id.splashScreenLayout);
		splashLayout.setVisibility(View.GONE);

        // Create a calendar object for each time reference
        mTargetTime = Calendar.getInstance();
        Calendar mCurrentTime = Calendar.getInstance();

        // Set target date and time variables to current date and time as a starting point
     	mTargetYear = mCurrentTime.get(Calendar.YEAR);
     	mTargetMonth = mCurrentTime.get(Calendar.MONTH);
     	mTargetDay = mCurrentTime.get(Calendar.DAY_OF_MONTH);	
		mTargetHour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
		mTargetMinute = mCurrentTime.get(Calendar.MINUTE);

		// Initialize event name input
		mEventNameDialog = new Dialog(this);
		mEventNameDialog.setContentView(R.layout.event_name_input);
		mEventNameInput = (EditText) mEventNameDialog.findViewById(R.id.eventNameInput);
		mEventNameOKButton = (Button) mEventNameDialog.findViewById(R.id.eventNameOKButton);
		initializeEventNameInput();

		// Initialize date picker
		mDatePickerDialog = new Dialog(this);
		mDatePickerDialog.setContentView(R.layout.date_picker_layout);
		mDatePicker = (DatePicker) mDatePickerDialog.findViewById(R.id.datePicker);
		mDatePickerOKButton = (Button) mDatePickerDialog.findViewById(R.id.datePickerOKButton);
		initializeDatePicker();

		// Initialize time picker
		mTimePickerDialog = new Dialog(this);
		mTimePickerDialog.setContentView(R.layout.time_picker_layout);
		mTimePicker = (TimePicker) mTimePickerDialog.findViewById(R.id.timePicker);
		mTimePickerOKButton = (Button) mTimePickerDialog.findViewById(R.id.timePickerOKButton);
		initializeTimePicker();

        // Add click listeners to the dialog buttons
		mEventNameOKButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	onEventNameOK();
            }
        });
		mEventNameInput.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	mEventNameInput.setText("");
            }
        });
        mDatePickerOKButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	onDatePickerOK();
            }
        });
        mTimePickerOKButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	onTimePickerOK();
            }
        });

		// Check if the splash has already been shown once.
        if (mHasNotShownSplash) {        	
        	showSplashScreen();
        } else {
        	// Check if there is already a running timer and, if so, display it.
        	checkCurrentTimer();
        }
    }

	/*
	 * "finish" forces app to close on stop.
	 * Fixes bug where a press on back button on first dialog - after coming back from a timer countdown - won't kill the app.
	 */
	protected void onStop() {
		super.onStop();
		
		// Better to manually dismiss any dialogs that may be open when app stops or we might get errors
		if (mEventNameDialog.isShowing()) {
			mEventNameDialog.dismiss();
		}
		if (mDatePickerDialog.isShowing()) {
			mDatePickerDialog.dismiss();
		}
		if (mTimePickerDialog.isShowing()) {
			mTimePickerDialog.dismiss();
		}
		
		finish();
	}
	
	private void launchTimerActivity() {
		// Save state to shared preferences
		utilities.saveToSharedPreferences(this, mEventName, mTargetTimeInMillis);

		// Start new intent and go to TimerActivity
		Intent intent = new Intent(this, TimerActivity.class);
		intent.putExtra("eventName", mEventName);
		intent.putExtra("targetTime", mTargetTimeInMillis);
		startActivityForResult(intent, 0);
	}    
	
	private void initializeEventNameInput() {
		// Initialize event name input
		mEventNameDialog.setTitle(R.string.info_eventName);
		
		// Dialogs use WRAP_CONTENT as default, so FILL_PARENT has to be set explicitly
		mEventNameDialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		// Set the default value for the text input
		mEventNameInput.setText("Min händelse!");
		
		// Workaround to exit the application on back button press
		mEventNameDialog.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				finish();
			}
		});
	}
	
	private void initializeDatePicker() {
		// Initialize date picker
		mDatePickerDialog.setTitle(R.string.info_datePicker);
		
		// Dialogs use WRAP_CONTENT as default, so FILL_PARENT has to be set explicitly
		mDatePickerDialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		// Set the default values for the picker
		mDatePicker.init(mTargetYear, mTargetMonth, mTargetDay, null);
		
		// Workaround to go back to previous dialog on back button press
		mDatePickerDialog.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				mEventNameDialog.show();
			}
		});
	}

	private void initializeTimePicker() {
		// Initialize timer picker
		mTimePickerDialog.setTitle(R.string.info_timePicker);
		mTimePicker.setIs24HourView(true);
		
		// Dialogs use WRAP_CONTENT as default, so FILL_PARENT has to be set explicitly
		mTimePickerDialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		// Set the default values for the picker
		mTimePicker.setCurrentHour(mTargetHour);
		mTimePicker.setCurrentMinute(mTargetMinute);
		
		// Workaround to go back to previous dialog on back button press
		mTimePickerDialog.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				mDatePickerDialog.show();
			}
		});
	}
	
	private void onEventNameOK() {
    	// Update event name variables
    	mEventName = String.valueOf(mEventNameInput.getText());
    	
    	// Validate input
    	validateNameInput();    	
	}
	
    private void onDatePickerOK() {
    	// Update date variables
    	mTargetYear = mDatePicker.getYear();
    	mTargetMonth = mDatePicker.getMonth();
    	mTargetDay = mDatePicker.getDayOfMonth();
    	
    	// Validate input
    	validateDateInput();
	}
	
	private void onTimePickerOK() {
		// Update time variables
		mTargetHour = mTimePicker.getCurrentHour();
		mTargetMinute = mTimePicker.getCurrentMinute();
		
    	// Validate input
    	validateCompleteTimeInput();
	}
    
	private void validateNameInput() {
		// Validation variables
		String errorMessage = (String) getText(R.string.eventNameErrorMessage);
		
		if (mEventName.equals("")) {
			validationErrorDialog(errorMessage);
		} else {
			mEventNameDialog.dismiss();
			mDatePickerDialog.show();
		}
	}
	
	private void validateDateInput() {
		// Validation variables
		String errorMessage = (String) getText(R.string.targetDateErrorMessage);
		
		// Get current time and date as calendar
		Calendar currentCalendar = Calendar.getInstance();
		
		// Create two numbers to compare to make sure that target date is larger than or equal to current date
		int comparisonNumberForCurrentDate = Integer.valueOf(currentCalendar.get(Calendar.YEAR) + utilities.padNumberWithZeroes(currentCalendar.get(Calendar.MONTH)) + utilities.padNumberWithZeroes(currentCalendar.get(Calendar.DAY_OF_MONTH)));
		int comparisonNumberForTargetDate = Integer.valueOf(mTargetYear + utilities.padNumberWithZeroes(mTargetMonth) + utilities.padNumberWithZeroes(mTargetDay));
		
		if (comparisonNumberForTargetDate < comparisonNumberForCurrentDate) {
			validationErrorDialog(errorMessage);
		} else {
			mDatePickerDialog.dismiss();
			mTimePickerDialog.show();
		}
	}
	
	private void validateCompleteTimeInput() {
		// Validation variables
		String errorMessage = (String) getText(R.string.targetTimeErrorMessage);
		Long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();
		
		// Set target time with input from user
		mTargetTime.set(mTargetYear, mTargetMonth, mTargetDay, mTargetHour, mTargetMinute);
		mTargetTimeInMillis = mTargetTime.getTimeInMillis();
		
		if (mTargetTimeInMillis <= currentTimeInMillis) {
			validationErrorDialog(errorMessage);
		} else {
			mTimePickerDialog.dismiss();
			launchTimerActivity();
		}
	}
	
	private void validationErrorDialog(String errorMessage) {
		new AlertDialog.Builder(this).setMessage(errorMessage)
			.setPositiveButton(getText(R.string.label_OKButton), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// Not implemented, simply close the dialog
				}
			}).show();
	}
	
	private void showSplashScreen() {
		splashLayout.setVisibility(View.VISIBLE);
		mHandler.postDelayed(hideSplashScreen, 3000);
	}

	private Runnable hideSplashScreen = new Runnable() {
		public void run() {
			// Hide splash and mark it as having been shown
			splashLayout.setVisibility(View.GONE);
			utilities.saveToSharedPreferences(InputActivity.this, false);
			
			// Check if there is already a running timer and, if so, display it.
			checkCurrentTimer();
		}
	};
	
	private void checkCurrentTimer() {
		if (utilities.existValuesInSharedPreferences(this)) {
        	launchTimerActivity();
        } else {
        	mEventNameDialog.show();
        }
	}

}
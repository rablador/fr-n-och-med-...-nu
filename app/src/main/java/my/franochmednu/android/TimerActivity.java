package my.franochmednu.android;

import java.util.Calendar;
import java.util.Map;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Activity that starts a timer and animation with durations based on 
 * the time left to a certain event.
 */
public class TimerActivity extends Activity {
	
	// Helper class for non-activity tasks
	Utilities utilities = new Utilities();
	
	// Timer variables
	private CountDownTimer mTimer;
	private long mTargetTimeInMillis;
	private String mEventName;
	
	// Various page element variables
	private TextView mTimerEventTitle;
	private TextView mTimerDaysDisplay;
	private TextView mTimerTimeOfDayDisplay;
	private TextView mTimerFinishedMessage;
	private Button mResetButton;
	private ImageView runner;

	// Animation variables
	private AnimationDrawable animation;
	
	// Alarm variables
	private AlarmManager alarmManager;
	PendingIntent pendingIntent;

	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.timer);

        // Initialize views
        mResetButton = (Button) findViewById(R.id.timerReset);
        mTimerDaysDisplay = (TextView) findViewById(R.id.timerDaysDisplay);
        mTimerTimeOfDayDisplay = (TextView) findViewById(R.id.timerTimeOfDayDisplay);
        mTimerFinishedMessage = (TextView) findViewById(R.id.timerFinishedMessage);
        mTimerEventTitle = (TextView) findViewById(R.id.timerEventTitle);
        runner = (ImageView) findViewById(R.id.runnerRunning);

        // Create a bundle and get extras
        Bundle extras = getIntent().getExtras();

        /*
         *  If there are no extras we assume that this activity was started by a notification.
         *  Else, run the timer activity as usual.
         */
        if (extras.containsKey("eventNameNotifier")) {
        	showFinishedTimer(extras);
        } else {
    		// Get extras and assign to variables
        	assignUserInputExtrasToVariables(extras);
	        
	        // Hide message for finished timer
	        mTimerFinishedMessage.setVisibility(View.GONE);
	        
	        // Set name for event
	        mTimerEventTitle.setText(mEventName);
	        
	        // Start the timer
	        startTimer();
        }
	}

	/**
	 * Takes the user back to InputActivity and cancels the current timer.
	 * Runs from an onClick in timer.xml.
	 */
	public void resetTimer(View view) {
		// Reset the data in shared preferences
		utilities.resetSharedPreferences(this);
		
		// Cancel the alarm (if set)
		if (pendingIntent != null) {
			cancelAlarm();
		}
		
		Intent intent = new Intent(this, InputActivity.class);
		startActivityForResult(intent, 0);
		finish();
	}
	
	private void assignUserInputExtrasToVariables(Bundle extras) {
		// Get extras and assign to variables
		mTargetTimeInMillis = extras.getLong("targetTime");
		mEventName = extras.getString("eventName");
	}

	private void startTimer() {
        // Get difference in time between user calendar and current time
		Calendar currentTime = Calendar.getInstance();
		Long currentTimeInMillis =  currentTime.getTimeInMillis();
        long timeLeftToZero = (mTargetTimeInMillis - currentTimeInMillis);
        
        /*
         * If time left is less than or equal to zero there will be need nothing to start,
         * so reset everything and go back to InputActivity. Normally the user will not get 
         * to TimerActivity without a proper time set, but crashes can sometimes "get around" 
         * this anyway.
         */
        if (timeLeftToZero <= 0) {
        	resetTimer(mResetButton);
        } else {
	        // Cancel timer if there is already one active
	        if (mTimer != null) {
	        	mTimer.cancel();
	        }
	        
	        // Set an alarm to let the user know when it's time
	       	setAlarm();
	        
	        // Start runner animation
	        startRunnerAnimation();

	        // Create and start timer with a 1 second interval
	        mTimer = new CountDownTimer(timeLeftToZero, 1000) {
	        	public void onTick(long millisUntilFinished) {
					// Parse milliseconds to days, hours, minutes and seconds
					Map<String, Integer> time = utilities.calculateTime(millisUntilFinished);
			        
			        // Display the timer
					if (time.get("days") == 1) {
						mTimerDaysDisplay.setText(time.get("days") + " " + getString(R.string.dayOrDays_day));
					} else {
						mTimerDaysDisplay.setText(time.get("days") + " " + getString(R.string.dayOrDays_days));
					}
					mTimerTimeOfDayDisplay.setText(
						utilities.padNumberWithZeroes((int) time.get("hours")) + ":" +
						utilities.padNumberWithZeroes((int) time.get("minutes")) + ":" +
						utilities.padNumberWithZeroes((int) time.get("seconds")));
				}
				public void onFinish() {
					// Timer actually stops on 00:00:01, so let's give it some help displaying that last zero
					mTimerTimeOfDayDisplay.setText("00:00:00");

					// Display finish message
					mTimerFinishedMessage.setVisibility(View.VISIBLE);
					
					// Change button text
					mResetButton.setText(getText(R.string.timerReset));
					
					// Stop animation and set a still runner image
					animation.stop();
					runner.setBackgroundResource(R.drawable.runner1);
					
					// Reset the data in shared preferences
					utilities.resetSharedPreferences(TimerActivity.this);
				}
			};
			mTimer.start();
	    }
	}
	
	private void showFinishedTimer(Bundle extras) {
		// Get and set event name
		String eventName = extras.getString("eventNameNotifier");
		mTimerEventTitle.setText(eventName);
		
		// Set the timers to 0
		mTimerDaysDisplay.setText("0 " + getString(R.string.dayOrDays_days));
		mTimerTimeOfDayDisplay.setText("00:00:00");
		
		// Change button text
		mResetButton.setText(getText(R.string.timerReset));
		
		// Set a still runner image
		runner.setBackgroundResource(R.drawable.runner1);
	}
	
	private void setAlarm() {
		// Create an intent for the alarm and send the event name as an extra
		Intent alarmIntent = new Intent(this, AlarmReceiver.class);
		alarmIntent.putExtra("eventName", mEventName);
		
		// Initialize the alarm manager with a pending intent and set the alarm
		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
		alarmManager.set(AlarmManager.RTC_WAKEUP, mTargetTimeInMillis, pendingIntent);
	}
	
	private void cancelAlarm() {
		alarmManager.cancel(pendingIntent);
	}
	
	private void startRunnerAnimation() {
		// Create animation
		runner.setBackgroundDrawable(animation);
    	runner.setBackgroundResource(R.drawable.animation);
    	animation = (AnimationDrawable) runner.getBackground();
    	
    	// Start animation
       	runner.post(animation);
	}
	
	public void onBackPressed() {
		finish();
	}

}
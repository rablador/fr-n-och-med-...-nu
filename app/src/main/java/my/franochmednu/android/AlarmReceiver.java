package my.franochmednu.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Handles the alarm and notification services.
 */
public class AlarmReceiver extends BroadcastReceiver {

	// Notification variables
	String mEventName;
	
	public void onReceive(Context context, Intent intent) {
		// Get event name from the intent
		mEventName = intent.getStringExtra("eventName");
		
		// Create a nice little toast and a permanent system notification to the let the user know that the time is up 
		Toast.makeText(context, mEventName, Toast.LENGTH_LONG).show();
		createSystemNotification(context);
	}
	
	private void createSystemNotification(Context caller) {
		// Create a notification manager
		NotificationManager mNotificationManager = (NotificationManager) caller.getSystemService(Context.NOTIFICATION_SERVICE);
		
		// Create a notification
		int icon = R.drawable.franochmednu_logo_not;
		CharSequence tickerText = caller.getText(R.string.appName) + " - " + mEventName;
		long when = 5000;
		Notification notification = new Notification(icon, tickerText, when);
		
		// Add some extra settings to the notification
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		
		// Set notification event info
		Context context = caller.getApplicationContext();
		CharSequence contentTitle = caller.getText(R.string.appName);
		CharSequence contentText = caller.getText(R.string.notificationMessage) + " " + mEventName + "!";
		Intent notificationIntent = new Intent(caller, TimerActivity.class);
		notificationIntent.putExtra("eventNameNotifier", mEventName);
		PendingIntent contentIntent = PendingIntent.getActivity(caller, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		
		// Add notification to the notification manager
		mNotificationManager.notify(0, notification);
	}
	
}
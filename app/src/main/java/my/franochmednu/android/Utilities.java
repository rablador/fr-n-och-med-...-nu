package my.franochmednu.android;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Various helper methods.
 * Meant to facility certain tasks that don't belong to a specific activity.
 */
public class Utilities {
	
	private static final String SHARED_PREFS_NAME = "timerState";

	/**
	 * Re-formats time and date numbers.
	 */
	public String padNumberWithZeroes(int integer) {
	    if (integer >= 10) {
	    	return String.valueOf(integer);
	    } else {
	    	return "0" + String.valueOf(integer);	    	
	    }
	}
	
	/**
	 * Converts milliseconds to days, hours, minutes and seconds.
	 */
	public Map<String, Integer> calculateTime(long millis) {
		Map<String, Integer> time = new HashMap<String, Integer>();
		long seconds = millis / 1000;
		
		time.put("days", (int) seconds / (3600 * 24));
        time.put("hours", (int) (seconds % (3600 * 24)) / 3600);
        time.put("minutes", (int) ((seconds % (3600 * 24)) % 3600) / 60);
        time.put("seconds", (int) ((seconds % (3600 * 24)) % 3600) % 60);
        
        return time;
	}
	
	/**
	 * Stores user input (state) to local storage for use when app resumes.
	 */
	public void saveToSharedPreferences(Context context, String eventName, Long targetTime) {
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("eventName", eventName);
		editor.putLong("targetTime", targetTime);
		editor.commit();
	}
	public void saveToSharedPreferences(Context context, Boolean hasNotShownSplash) {
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean("hasNotShownSplash", hasNotShownSplash);
		editor.commit();
	}

	/**
	 * Reads stored user input (state) from local storage for use when app resumes.
	 */
	public String readStringFromSharedPreferences(Context context, String key) {
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		return sp.getString(key, "");
	}
	public long readLongFromSharedPreferences(Context context, String key) {
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		return sp.getLong(key, (long) 0);
	}
	public boolean readBooleanFromSharedPreferences(Context context, String key) {
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
		return sp.getBoolean(key, true);
	}
	
	/**
	 * Resets stored user input (state) in local storage to avoid data fetching when app resumes.
	 */
	public void resetSharedPreferences(Context context) {
		saveToSharedPreferences(context, "", (long) 0);
	}
	
	/**
	 * Checks if there are stored values in shared preferences.
	 */
	public boolean existValuesInSharedPreferences(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		String eventName = sp.getString("eventName", "");
		long targetTimeInMillis = sp.getLong("targetTime", (long) 0);
		
		if (!eventName.equals("") && !(targetTimeInMillis == (long) 0)) {
			return true;
		} else {
			return false;
		}
	}
}